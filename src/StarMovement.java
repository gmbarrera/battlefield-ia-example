import java.util.ArrayList;

import ia.battle.core.BattleField;
import ia.battle.core.FieldCell;
import ia.battle.core.actions.Move;
import ia.battle.util.AStar;

public class StarMovement extends Move {

	private FieldCell from, to;

	public StarMovement() {
		
	}
	
	public StarMovement(FieldCell from, FieldCell to) {
		this.from = from;
		this.to = to;
	}
	
	@Override
	public ArrayList<FieldCell> move() {
		AStar astar = new AStar(BattleField.getInstance().getMap());
		
		ArrayList<FieldCell> path = astar.findPath(this.from, this.to);
		
		return path;
	}

}
